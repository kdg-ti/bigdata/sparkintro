import org.apache.hadoop.util.Time;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.util.DateTimeUtils;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.LongAccumulator;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.apache.spark.sql.functions.col;

public class SparkExamples {
    private SparkSession spark;
    private String filePath;

    public SparkExamples() {
        //1.  https://spark.apache.org/docs/latest/quick-start.html#self-contained-applications
        // TODO: Externalise master configuration
         spark = SparkSession.builder().appName("Simple Application").master("local[4]").getOrCreate();
    }

    public void rddIntro()  {
        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#initializing-spark
        JavaSparkContext sc = JavaSparkContext.fromSparkContext(spark.sparkContext());
        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#parallelized-collections
        List<Integer> data = Arrays.asList(1, 2, 3, 4, 5);
        JavaRDD<Integer> distData = sc.parallelize(data);
        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#transformations
        // Spark heeft lazy transformations
        JavaRDD<Integer> distMappedData = distData.map(i -> i*10);
        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#actions
        // Als op een reeks transformaties geen acties volgen is er geen nood om de transformaties uit te voeren.
        distMappedData.collect();

        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#understanding-closures-
        // Je kan niet zomaar tellen over de concurrent tasks (samenlopende) heen, de tasks krijgen elk een copy van de counter.
        // Array gebruikt omdat een gewone int niet werkt
        final int counter[] ={0};
        distData.foreach(x -> counter[0]+=x);
        System.out.println("Counter zou gelijk moeten zijn aan " + data.stream().reduce( (a,b)-> a+b).orElse(0) +" maar is "+ counter[0]);

        // Je kan in plaats daarvan wel een accumulator gebruiken
        LongAccumulator accum = sc.sc().longAccumulator("Counter");
        distData.foreach(x -> accum.add(x));
        System.out.println("Accumulator zou gelijk moeten zijn aan " + data.stream().reduce( (a,b)-> a+b).orElse(0) +" en is "+ accum.value());

        //https://spark.apache.org/docs/latest/rdd-programming-guide.html#external-datasets
        //Externe bestanden inlezen
        //TODO: Make filepath configurable
        JavaRDD<String> sqlStrings = sc.textFile("file:///C:/temp/Velo/*.sql");
        JavaRDD<String> insertStrings = sqlStrings.filter(l -> l.toLowerCase().contains("insert into"));
        System.out.println("Er zijn " + insertStrings.count() + " inserts in de files");
    }

    public void SQLLikeIntro(){
        //https://spark.apache.org/docs/latest/sql-getting-started.html#creating-dataframes
        Dataset<Row> df = spark.read().json("file:///C:/sparkIO/input");
        //df.show();
        //https://spark.apache.org/docs/latest/sql-getting-started.html#untyped-dataset-operations-aka-dataframe-operations
        //df.printSchema();
        //df.select(col("name")).show();
        //df.select(col("name"), col("age").plus(1)).show();
        //df.filter(col("age").gt(21)).show();
        //df.groupBy("age").count().show();

        //https://spark.apache.org/docs/latest/sql-getting-started.html#running-sql-queries-programmatically
       /* df.createOrReplaceTempView("people");

        Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
        sqlDF.show();
        spark.sql("SELECT * FROM people where name='Michael'").show();
*/
        //https://spark.apache.org/docs/latest/sql-getting-started.html#creating-datasets
        List<Person> pList = new ArrayList<>();
        pList.add(new Person("Andy",32));
        pList.add(new Person("Jos",32));
        pList.add(new Person("Maai",18));
        pList.add(new Person("Daisy",19));
        pList.add(new Person("Sam",23));

        // Encoders are created for Java beans
        Encoder<Person> personEncoder = Encoders.bean(Person.class);
        Dataset<Person> javaBeanDS = spark.createDataset(pList,personEncoder);
        javaBeanDS.show();

        Dataset<Person> peopleDS = spark.read().json("file:///C:/sparkIO/output" + Time.now()).as(personEncoder);
        peopleDS.show();
        peopleDS.schema();

    }

    public void StructuredStreamingIntro() throws StreamingQueryException {
        //TODO make everything configurable
        spark.conf().set("spark.sql.shuffle.partitions","2");
        spark.conf().set("spark.sql.streaming.checkpointLocation",("file:///C:/sparkIO/checkpoint"));
        // INFO: In de checkpoint folder wordt alle state informatie bijgehouden
        //TODO shuffle configureerbaar maken.
        


        Encoder<Person> personEncoder = Encoders.bean(Person.class);
        //Lees een stream readStream() in plaats van read() bij batch. Vanaf nu ben je structured streaming aan het toepassen
        Dataset<Person> lines = spark.readStream().schema(personEncoder.schema()).json("file:///C:/sparkIO/streaminput").as(personEncoder);

        // INFO: Mogelijkheid 1: gebruik ingebouwde groupBy en sum functionaliteit.
        Dataset<Row> totals = lines.groupBy("name").count();

        // INFO: Mogelijkheid 2: gebruik sql
        //lines.createOrReplaceTempView("people");
        //Dataset<Row> totals2 = spark.sql("select name, sum(salary) from people group by name");

        // INFO: DataStreamWriter schrijft de data op de stroom naar een externe opslag weg.
        // INFO: Ter illustriatie is dit naar de console
        // INFO: outputMode(complete) wil zeggen dat de volledige output (van alle voorgaande gegevens) wordt weggeschreven.
        // INFO: update wil zeggen dat enkel wijzigingen sinds laatste trigger wordt weggeschreven.
        // INFO: append mode: Enkel nieuwe rijen worden weggeschreven. Er mogen dus geen wijzigingen gebeuren aan een bestaande rij.
        //      LET OP: aggregate zorgt voor updates bestaande rijen. Voor een file sink moet je dus zorgen dat er geen updates kunnen komen.
        //              ENKEL MOGELIJK MET WATERMARK!!!
        StreamingQuery query = null;
        try {
            query = totals.writeStream().outputMode(OutputMode.Update())
                    .format("console").option("path","file:///C:/sparkIO/streamingoutput").start();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }


        query.awaitTermination();
    }

    public void waitToExitProgram(){
        try {
            Thread.sleep(1000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
